回调函数  
========


callback(err, data, cobject)  
--------
参数说明：
* `err`{Object} - 如果有错误，则为错误信息，否则为null
* `data`{cObject} - 如果有无错误，则为收到信息，否则为null  
* `cobject`{cObject} - 处理函数等构成的对象，格式详见[cObject](./cobject.md)  
