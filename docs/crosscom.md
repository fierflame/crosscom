crosscom实例  
========

crosscom.addListener(type, func)  
--------
添加一个监听  
参数说明  
* `type`{String} - 监听的类型  
* `func`{listener Function} - 用于处理的函数，格式详见[监听函数](./listener.md)  


crosscom.removeListener(type, func)  
--------
移除一个监听  
参数说明  
* `type`{String} - 监听的类型  
* `func`{listener Function} - 用于处理的函数  

async crosscom.send({data, type[, source]}[, cb])  
--------
`异步`发送一个消息  
异步需要系统支持Promise  
参数说明  
* `opt.data`{Object} - 消息内容  
* `opt.type`{Process} - 发送的类型，在crosscom.addListener中注册时，相同的type可以处理此消息  
* `opt.source`{Window} - *可选的*要与之通信的页面，在浏览器环境中为必须项，在nodejs环境中无效  
* `cb`{callback Function} - *可选的*要与之通信的进程，格式详见[回调函数](./callback.md)  

返回值或异常均为[cObject](./cobject.md)，其中data分别为返回结果和错误信息  


crosscom.request({data, type[, source]}[, cb])  
--------
crosscom.send({data, type[, source]}[, cb])的别名  


crosscom.on(type, func)  
--------
crosscom.addListener(type, func)的别名  


crosscom.off(type, func)  
--------
crosscom.removeListener(type, func)的别名  


crosscom.router  
--------
监听函数  
nodejs环境和浏览器原生JavaScript环境中，已经设置好，无效手动设置；浏览器requirejs环境中，需要手动设置到 window.onmessage  
