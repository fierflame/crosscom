cObject  
========
cObject.source  
--------
发送消息的源，仅浏览器环境下有效  

cObject.data  
--------
消息数据  
回调函数中无效  


cObject.type  
--------
消息类型  


async cObject.response(data[, msgtype][, notNeedRet][, cb])  
--------
`异步`返回数据  
异步需要系统支持Promise  
参数说明  
* `data`{Object} - 消息内容  
* `type`{String} - 发送的类型，在crosscom.addListener中注册时，相同的type可以处理此消息  
* `notNeedRet`{Boolean} - *可选的*是否不需要返回值，默认为false  
* `cb`{callback Function} - *可选的*要与之通信的进程，格式详见[回调函数](./callback.md)  

cObject.next()  
--------
采用下一个监听函数处理  
