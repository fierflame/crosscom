初始化函数  
========
初始化函数只用在nodejs中，浏览器环境中，将直接得到一个[实例](./crosscom.md)  

init(process)
--------
创建一个crosscom实例  
参数说明  
* `process`{Process} - 要与之通信的进程  

返回值为一个[crosscom实例](./crosscom.md)  
