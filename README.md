crosscom  
========
crosscom是nodejs进程间及浏览器非同域页面间的Callback方式及Promise方式异步通信解决方案。  

支持的环境  
--------
1. nodejs环境  
2. 浏览器requirejs环境  
3. 浏览器原生JavaScript环境  

nodejs环境与浏览器环境的区别  
--------
1. 导出对象不同  
   * nodejs中，得到是一个初始化函数，需要一个process对象作为参数才能得到实例  
   * 浏览器中，得到的是一个实例  
2. send方法是否需要source参数  
   * nodejs中，因为初始化时已经传入一个process对象，此process对象将作为通信中的source，故不需要source参数  
   * 浏览器中，因为不需要用户初始化，不确定目标所以需要一个source参数  
3. 实例的通信范围不同  
   * nodejs中，进程通信是基于通道的原因，所以每一个实例只能与指定进程进行通信  
   * 浏览器中，因为实例的send方法中需要一个source参数作为目标，所以一个实例可以与任意页面进行通信  

其他说明
--------
1. 虽然crosscom在浏览器方面是为解决非同域页面间异步通信提供提供更好的解决方案，但是也同时支持用于同域页面  

开发文档目录  
--------
* [初始化函数](./docs/init.md)  
* [crosscom实例](./docs/crosscom.md)  
* [监听函数](./docs/listener.md)  
* [回调函数](./docs/callback.md)  
* [cObject](./docs/cobject.md)  
